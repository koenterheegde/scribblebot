# To declare the pins in a central place. Makes it easy to swap pins.


class Pins(object):
    NUCLEO_BLUE_BUTTON = 'C13'
    BR_BUTTON1 = ''
    POTMETER1 = 'A5'
    POTMETER2 = 'A5'
    MOTOR1_DIRECTION = 'D4'
    MOTOR1_PWM = 'D5'
    MOTOR1_ENC_A = 'D0'
    MOTOR1_ENC_B = 'D1'
    MOTOR2_DIRECTION = 'D7'
    MOTOR2_PWM = 'D6'
    MOTOR2_ENC_A = 'D11'
    MOTOR2_ENC_B = 'D12'

