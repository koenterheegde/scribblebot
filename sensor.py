from switch import BlueSwitch 
from pin_definitions import Pins
from biorobotics import AnalogIn

class SensorState(object):

    def __init__(self):
        self.switch_value = 0
        self.blue_switch = BlueSwitch()
        self.potmeter_1_value = 0

        return



    def update(self):
        self.switch_value = self.blue_switch.value()
        self.potmeter_test = AnalogIn(Pins.POTMETER1)
        self.potmeter_1_value = self.potmeter_test.read()
        return