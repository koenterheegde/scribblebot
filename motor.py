from machine import Pin
from biorobotics import PWM
from pin_definitions import Pins

class Motor(object):
    

    def __init__(self, frequency, motor = 1):
        if motor == 1:
            self.pwm = PWM(Pins.MOTOR1_PWM, frequency)
            self.direction = Pin(Pins.MOTOR1_DIRECTION, Pin.OUT)

        else:
            self.pwm = PWM(Pins.MOTOR2_PWM, frequency)
            self.direction = Pin(Pins.MOTOR2_DIRECTION, Pin.OUT)

        return

    
    def write(self, pwm_value):

        if pwm_value < 0:
            self.direction.value(1)
        
        else: 
            self.direction.value(0)

        self.pwm.write(abs(pwm_value))
        return