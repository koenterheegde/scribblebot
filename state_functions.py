from sensor import SensorState
from states import State
from leds import Leds
from motor import Motor
from biorobotics import SerialPC

class StateFunctions(object):

    def __init__(self, robot_state, sensor_state, ticker_frequency):

        self.robot_state = robot_state
        self.sensor_state = sensor_state
        self.leds = Leds()
        self.motor_1 = Motor(ticker_frequency)
        self.pc = SerialPC(1)

        self.callbacks = {
            State.SAFE: self.safe, 
            State.READ: self.read,
            State.ON: self.on}

        return

    
    def safe(self):

        # Entry action
        if self.robot_state.is_changed():
            self.robot_state.set(State.SAFE)
            self.leds.green.on()

            print('SAFE')

        # Action
        self.motor_1.write(0)

        # State guard and exit action
        if self.sensor_state.switch_value:
            self.robot_state.set(State.READ)

        return

    def read(self):

        # Entry action
        if self.robot_state.is_changed():
            self.robot_state.set(State.READ)
            self.leds.yellow.on()

            print('READ')

        # Action 

        print('The potmeter value is {0}'.format(self.sensor_state.potmeter_1_value))
        
        self.motor_1.write(0)
        self.pc.set(0, self.sensor_state.potmeter_1_value)
        self.pc.send()

        # State guard and exit action
        if self.sensor_state.switch_value:
            self.robot_state.set(State.ON)

        return

    def on(self):

        # Entry action
        if self.robot_state.is_changed():
            self.robot_state.set(State.ON)
            self.leds.yellow.off()
            self.leds.red.on()
            print('Entered ON')

        # Action
        self.motor_1.write(2 * (self.sensor_state.potmeter_1_value - 0.5))


        # State guard and exit action
        if self.sensor_state.switch_value:
            self.robot_state.set(State.SAFE)

        return



